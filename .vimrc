" This will enable code folding.
" Use the marker method of folding.
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

let mapleader =" "
set number relativenumber

"Remaps --------------{{{
imap jj <Esc>
nnoremap ss :wa<cr>

nmap <leader>n :Vexplore<cr>
nmap <leader>e :Explore<cr>
nmap <leader>f :Files<cr>

" Install gvim to enable + register
vnoremap <C-c> "+y 
map <C-v> "+P 
" }}}

" Start gruvbox ----------{{{
autocmd vimenter * colorscheme gruvbox
set bg=dark
set t_Co=256 " it needs to run gruvbox in tmux
" }}}

" Commands --------------{{{
map <leader>s :norm yiw<cr> :%s/<C-r>"//g<left><left> " Substituion

map <leader>q <cr>BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html Prettier
" autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html Prettier

" }}}
" Random config ---------------------------------------------------- {{{
filetype on
filetype indent on
" Set shift width to 4 spaces.
set shiftwidth=2

" set tab width to 4 columns.
set tabstop=2

" use space characters instead of tabs.
set expandtab

" do not save backup files.
set nobackup

" do not let cursor scroll below or above n number of lines when scrolling.
set scrolloff=10

" do not wrap lines. allow long lines to extend as far as the line goes.
set nowrap

" while searching though a file incrementally highlight matching characters
" as you type.
set incsearch

" ignore capital letters during search.
set ignorecase

" override the ignorecase option if searching for capital letters.
" this will allow you to search specifically for capital letters.
set smartcase

" show partial command you type in the last line of the screen.
set showcmd

" show the mode you are on the last line.
set showmode

" show matching words during a search.
set showmatch

" use highlighting when doing a search.
set hlsearch

 " set the commands to save in history default number is 20.
set history=1000

" execut prettier when save
autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html Prettier

" configure javascript
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_ngdoc = 1
let g:javascript_plugin_flow = 1
" augroup javascript_folding
"     au!
"     au FileType javascript setlocal foldmethod=syntax
" augroup END
"
" }}}

" PLUGINS ---------------------------------------------------------------- {{{

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'tpope/vim-commentary'
Plug 'pangloss/vim-javascript'
Plug 'othree/yajs.vim'
Plug 'prettier/vim-prettier', {  
            \ 'do': 'yarn install', 
            \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }
Plug 'valloric/youcompleteme'
Plug 'pechorin/any-jump.vim'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

call plug#end()

" }}}
