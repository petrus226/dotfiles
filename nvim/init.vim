let mapleader =" "
set number relativenumber

"Remaps --------------{{{
imap jj <Esc>
nnoremap ss :wa<cr>

nmap <leader>n :Vexplore<cr>
nmap <leader>e :Explore<cr>
" }}}

" Start gruvbox ----------{{{
autocmd vimenter * colorscheme gruvbox
set bg=dark
set t_Co=256 " it needs to run gruvbox in tmux
" }}}

" Commands --------------{{{
map <leader>s :norm yiw<cr> :%s/<C-r>"//g<left><left> " Substituion
" }}}
" Random config ---------------------------------------------------- {{{
filetype on
filetype indent on
" Set shift width to 4 spaces.
set shiftwidth=4

" set tab width to 4 columns.
set tabstop=4

" use space characters instead of tabs.
set expandtab

" do not save backup files.
set nobackup

" do not let cursor scroll below or above n number of lines when scrolling.
set scrolloff=10

" do not wrap lines. allow long lines to extend as far as the line goes.
set nowrap

" while searching though a file incrementally highlight matching characters
" as you type.
set incsearch

" ignore capital letters during search.
set ignorecase

" override the ignorecase option if searching for capital letters.
" this will allow you to search specifically for capital letters.
set smartcase

" show partial command you type in the last line of the screen.
set showcmd

" show the mode you are on the last line.
set showmode

" show matching words during a search.
set showmatch

" use highlighting when doing a search.
set hlsearch

 " set the commands to save in history default number is 20.
set history=1000

" }}}

"Telescope{{{
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
"}}}


" PLUGINS ---------------------------------------------------------------- {{{

call plug#begin('~/.config/nvim/plugged')
Plug 'neovim/nvim-lspconfig' " Configuration for Nvim LSP --> https://blog.devgenius.io/setting-up-lsp-config-in-neovim-for-beautiful-error-diagnostics-26d40bbe40a2
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-commentary'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.0' } " https://github.com/nvim-telescope/telescope.nvim
Plug 'motemen/git-vim'
call plug#end()
" }}}
